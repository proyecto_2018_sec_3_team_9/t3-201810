package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface ILinkedList <T extends Comparable<T>>  
{

	/**
	 * Adds a new element to the list by adding it first to a node and adding that node to the list.
	 * @param element The element you want to add
	 */
	public void add(T element);
	
	/**
	 * Deletes the element received by parameter from the list.
	 * @param element The element you want to delete
	 * @return true if the element exists and was correctly removed, false if not.
	 */
	public boolean remove(T element);
	
	/**
	 * Gets the element given by parameter from the list.
	 * @param element The element you want to get.
	 * @return The element. Null if it doesn't exist.
	 */
	public T get(T element);
	
	/**
	 * Gives the size of the list
	 * @return The number of nodes in the list.
	 */
	public int size();
	
	/**
	 * Returns the element found on the given position
	 * @param pos The position you wanna get the element from
	 * @return The element on that position.
	 */
	public T get(int index);
	
	/**
	 * Sets the listing of elements at the first element.
	 */
	public void listing();
	
	/**
	 * Returns the current element T in the listing
	 * @return Null if it doesn't exist.
	 */
	public T getCurrent();
	
	/**
	 * Advances to next element in the listing.
	 * @return The next element (if it exists).
	 */
	public T next();

	/**
	 * Says if there's a next node on the list
	 * @return true if there's a next node, false if not
	 */
	boolean hasNext();

	/**
	 * Deletes the element found int the index given from the list.
	 * @param index The index of the element you want to delete.
	 * @return true if the element exists and was correctly removed, false if not.
	 */
	void remove(int index);

	/**
	 * Sets the element found on the given index to the element given by parameter
	 * @param element The element you want to insert
	 * @param index The index where it will be inserted
	 * @throws IndexOutOfBoundsException If the index is out of the list's bounds
	 */
	void set(T element, int index) throws IndexOutOfBoundsException;
	
	T previous();

	boolean hasPrevious();

}
