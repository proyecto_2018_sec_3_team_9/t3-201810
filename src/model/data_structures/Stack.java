package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<T extends Comparable<T>> implements IStack<T>, Iterable<T>
{
	/**
	 * The first node in the stack.
	 */
	private Node<T> first;     
	
	/**
	 * Number of elements n in the stack.
	 */
	private int n;               

	/**
	 * Initializes an empty stack.
	 */
	public Stack() 
	{
		first = null;
		n = 0;
	}

	/**
	 * Adds the item to this stack.
	 * @param  item the item to add
	 */
	@Override
	public void push(T item) 
	{
		Node<T> oldfirst = first;
		first = new Node<T>(item);
		first.changeNext(oldfirst);
		n++;
	}

	/**
	 * Removes and returns the item most recently added to this stack.
	 * @return the item most recently added
	 * @throws NoSuchElementException if this stack is empty
	 */
	@Override
	public T pop() 
	{
		if (isEmpty()) throw new NoSuchElementException("Stack underflow");
		T item = first.getElement();        // save item to return
		first = first.getNext();            // delete first node
		n--;
		return item;     
	}

	/**
	 * Returns true if this stack is empty.
	 * @return true if this stack is empty; false otherwise
	 */
	@Override
	public boolean isEmpty() 
	{
		boolean isEmpty = false;
		if(first == null)
		{
			isEmpty = true;
		}
		return isEmpty;
	}

	/**
	 * Returns the number of items in this stack.
	 * @return the number of items in this stack
	 */
	public int size() 
	{
		return n;
	}


	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return null;
	}
}
