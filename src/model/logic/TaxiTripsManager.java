package model.logic;

import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.TimeZone;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private Queue<Service> cola;
	private Stack<Service> pila;


	public void loadServices(String serviceFile, String taxiId) 
	{
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		System.out.println("Cargar sistema con" + serviceFile);

		cola = new Queue<Service>();
		pila = new Stack<Service>();
		JsonParser parser = new JsonParser();
		try {
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));
			SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss'.'000");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null)
				{
					System.out.println("The object is null in:" + i);
				}
				String taxid = "NaN";
				if ( obj.get("taxi_id") != null )
				{ taxid = obj.get("taxi_id").getAsString(); }

				//						System.out.println("------------------------------------------------------------------------------------------------");
				//						System.out.println(obj);

				if(taxid.equals(taxiId)) {

					String company = "NaN";
					if ( obj.get("company") != null )
					{ company = obj.get("company").getAsString();}

					Taxi taxi = new Taxi(taxiId, company);

					String doCensus = "NaN";
					if ( obj.get("dropoff_census_tract") != null )
					{ doCensus = obj.get("dropoff_census_tract").getAsString(); }

					String doLatitude = "NaN";
					if ( obj.get("dropoff_centroid_latitude") != null )
					{ doLatitude = obj.get("dropoff_centroid_latitude").getAsString(); }

					String doLongitude = "NaN";
					if ( obj.get("dropoff_centroid_longitude") != null )
					{ doLongitude = obj.get("dropoff_centroid_longitude").getAsString(); }

					JsonObject dropoff_localization_obj = null; 

					/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
					if ( obj.get("dropoff_centroid_location") != null )
					{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

					/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
					JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

					/* Obtener cada coordenada del JsonArray como Double */
					double longitude = dropoff_localization_arr.get(0).getAsDouble();
					double latitude = dropoff_localization_arr.get(1).getAsDouble();
					// System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");
					}
					else
					{
						// System.out.println( "[Lon: NaN, Lat: NaN ]");
					}

					int doCommunity = 0;
					if ( obj.get("dropoff_community_area") != null )
					{ doCommunity = Integer.parseInt(obj.get("dropoff_community_area").getAsString()); }

					//					double extras = 0;
					//					if ( obj.get("extras") != null )
					//					{ extras = Double.parseDouble(obj.get("extras").getAsString()); }
					//
					//					double fare = 0;
					//					if ( obj.get("fare") != null )
					//					{ fare = Double.parseDouble(obj.get("fare").getAsString()); }
					//
					//					String paymentType = "NaN";
					//					if ( obj.get("payment_type") != null )
					//					{ doLongitude = obj.get("payment_type").getAsString(); }
					//
					//					String puCensus = "NaN";
					//					if ( obj.get("pickup_census_tract") != null )
					//					{ puCensus = obj.get("pickup_census_tract").getAsString(); }
					//
					//					String puLatitude = "NaN";
					//					if ( obj.get("pickup_centroid_latitude") != null )
					//					{ puLatitude = obj.get("pickup_centroid_latitude").getAsString(); }
					//
					//					String puLongitude = "NaN";
					//					if ( obj.get("pickup_centroid_longitude") != null )
					//					{ puLongitude = obj.get("pickup_centroid_longitude").getAsString(); }
					//
					//					JsonObject pickup_localization_obj = null; 
					//
					//					/* Obtener la propiedad pickup_centroid_location (JsonObject) de un servicio*/
					//					if ( obj.get("pickup_centroid_location") != null )
					//					{ pickup_localization_obj =obj.get("pickup_centroid_location").getAsJsonObject();
					//
					//					/* Obtener la propiedad coordinates (JsonArray) de la propiedad pickup_centroid_location (JsonObject)*/
					//					JsonArray pickup_localization_arr = pickup_localization_obj.get("coordinates").getAsJsonArray();
					//
					//					/* Obtener cada coordenada del JsonArray como Double */
					//					double longitude = pickup_localization_arr.get(0).getAsDouble();
					//					double latitude = pickup_localization_arr.get(1).getAsDouble();
					//					// System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");
					//					}
					//					else
					//					{
					//						// System.out.println( "[Lon: NaN, Lat: NaN ]");
					//					}
					//
					//					int puCommunity = 0;
					//					if ( obj.get("pickup_community_area") != null )
					//					{ puCommunity = Integer.parseInt(obj.get("pickup_community_area").getAsString()); }
					//
					//					double tips = 0;
					//					if ( obj.get("tips") != null )
					//					{ tips = Double.parseDouble(obj.get("tips").getAsString()); }
					//
					//					double tolls = 0;
					//					if ( obj.get("tolls") != null )
					//					{ tolls = Double.parseDouble(obj.get("tolls").getAsString()); }
					//
					//					String tripEnd = "NaN";
					//					if ( obj.get("trip_end_timestamp") != null )
					//					{ tripEnd = obj.get("trip_end_timestamp").getAsString(); }

					String tripId = "NaN";
					if ( obj.get("trip_id") != null )
					{ tripId = obj.get("trip_id").getAsString(); }

					int seconds = 0;
					if ( obj.get("trip_seconds") != null )
					{ seconds = Integer.parseInt(obj.get("trip_seconds").getAsString()); }

					double miles = 0;
					if ( obj.get("trip_miles") != null )
					{ miles = Double.parseDouble(obj.get("trip_miles").getAsString()); }

					GregorianCalendar tripStart = new GregorianCalendar();
					if ( obj.get("trip_start_timestamp") != null )
					{ String date = obj.get("trip_start_timestamp").getAsString();
					tripStart.setTime(sdf.parse(date));}

					System.out.println(tripStart.getTimeInMillis());

					double total = 0;
					if ( obj.get("trip_total") != null )
					{ total = Double.parseDouble(obj.get("trip_total").getAsString()); }

					Service service = new Service(tripId, taxiId, seconds, miles, total, tripStart);
					//						System.out.println(taxi.getCompany());
					cola.enqueue(service);
					pila.push(service);
				}

			} 

			System.out.println("el tamaño de la lista de servicios es:" + cola.size());
			System.out.println("el tamaño de la lista de taxis es:" + pila.size());
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public int [] servicesInInverseOrder() 
	{
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder:" +pila.size());
		int [] resultado = new int[2];
		int ordenados = 0;
		int desordenados = 0;
		if(pila.size() == 1) {
			ordenados++;
		}
		else if(pila.size()>1) {

			while(!pila.isEmpty()) {
				Service s1 = pila.pop();
				Service s2 = pila.pop();
				boolean esMayor = s1.getTripStartTime().compareTo(s2.getTripStartTime())>=0;
				if(esMayor) {

					ordenados++;
					pila.push(s2);
					if(pila.size()==1) {
						ordenados++;
						break;
					}
				}
				else {
					desordenados++;
					pila.push(s1);
					if(pila.size()==1) {
						desordenados++;
						break;
					}
				}

			}

		}
		resultado[0] = ordenados;
		resultado[1] = desordenados;
		return resultado;
	}

	@Override
	public int [] servicesInOrder() 
	{
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder:" +cola.size());
		int [] resultado = new int[2];
		int ordenados =  0;
		int desordenados = 0;
		Queue<Service> aux = new Queue<Service>();

		if(cola.size() == 1) 
		{
			ordenados++;
		}
		else if(cola.size() >1) 
		{
			try{
				while(!cola.isEmpty()) 
				{
					if(aux.isEmpty())
					{

						Service temp1 = cola.dequeue();
						Service temp2 = cola.dequeue();
						boolean esMenor = temp1.getTripStartTime().compareTo(temp2.getTripStartTime()) <= 0;
						if(esMenor) {
							ordenados++;
							aux.enqueue(temp2);						
						}
						else {
							desordenados++;
							aux.enqueue(temp1);
						}

					}
					else
					{
						Service temp1 = aux.dequeue();
						Service temp2= cola.dequeue();
						boolean esMenor = temp1.getTripStartTime().compareTo(temp2.getTripStartTime()) <= 0;
						if(esMenor){
							ordenados++;
							aux.enqueue(temp2);
							if(cola.size()==0) {
								ordenados ++;
							}
						}
						else
						{
							desordenados++;
							aux.enqueue(temp1);
							if(cola.size()==0) {
								desordenados++;
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			resultado[0] = ordenados;
			resultado[1] = desordenados;	
		}
		return resultado;
	}
}
