package model.vo;

import java.util.GregorianCalendar;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String trip_id;
	
	private String taxi_id;
	
	private int trip_seconds;
	
	private double trip_miles;
	
	private double trip_total;
	
	private GregorianCalendar start_timestamp;
	
	public Service(String trip_id, String taxi_id, int trip_seconds, double trip_miles, double trip_total, GregorianCalendar start_timestamp)
	{
		this.trip_id = trip_id;
		this.taxi_id = taxi_id;
		this.trip_seconds = trip_seconds;
		this.trip_miles = trip_miles;
		this.trip_total = trip_total;
		this.start_timestamp = start_timestamp;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public GregorianCalendar getTripStartTime() {
		// TODO Auto-generated method stub
		return start_timestamp;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
