package model.data_structures.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Node;
import model.vo.Service;

public class NodeTest 
{
	/**
	 *A new node to be used in the test.
	 */
	private Node<Service> node;

	/**
	 * a new service element to be in the node.
	 */
	private Service service;

	/**
	 * Creates a new Service and a node with it. This method should be used in every test.
	 */
	@Before
	public void setupEscenary( )
	{
		service = new Service("1234", "abcd", 36, 23.5, 7.6, null);
		node = new Node<Service>(service);
	}

	/**
	 * Creates a new Service and a node with it. This method should be used in every test.
	 */
	@Before
	public void setupEscenary2( )
	{
		service = new Service("5678", "efgh", 56, 28.7, 8.2, null);
		node = new Node<Service>(service);
	}

	/**
	 * Test 1: Verifies method changeNext(). <br>
	 * <b>Methds to prove:</b> <br>
	 * Node<br>
	 * getNext<br>
	 * changeNext<br>
	 * <b> Cases: </b><br>
	 * 1) No next node. <br>
	 * 2) There is a next node.
	 */
	@Test
	public void testChangeNext( )
	{

		Service service2 = new Service("1357", "aceg", 22, 13.5, 5.5, null);
		setupEscenary();
		// Case1
		Node<Service> node1 = new Node<Service>(service2);
		assertNull( "The next element should be null.", node.getNext( ) );
		node.changeNext(node1);
		assertNotNull( "The next element should exist.", node.getNext( ));
		assertEquals( "The next element doesn´t match.", node1.getElement(), node.getNext().getElement());


		// Case 2
		Service service3 = new Service("2468", "bdfh", 24, 16.5, 7.5, null);
		setupEscenary2();
		Node<Service> node2 = new Node<Service>(service3);
		node.changeNext(node2);
		assertNotNull( "El elemento anterior debe existir pues ha sido asignado.", node.getNext());
		assertEquals( "El elemento anterior no coincide", node2.getElement(), node.getNext().getElement());
	}
}
