package model.data_structures.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;
import model.vo.Taxi;

public class QueueTest 
{
	/**
	 * A new queue.
	 */
	private Queue<Taxi> queue;

	/**
	 * A new taxi1 in the queue.
	 */
	private Taxi taxi1;
	
	/**
	 * A new taxi2 in the queue.
	 */
	private Taxi taxi2;
	
	/**
	 * A new taxi3 in the queue.
	 */
	private Taxi taxi3;
	
	/**
	 * A new taxi4 in the queue.
	 */
	private Taxi taxi4;
	
	/**
	 * A new taxi5 in the queue.
	 */
	private Taxi taxi5;

	// -----------------------------------------------------------------
	// Escenaries
	// -----------------------------------------------------------------

	/**
	 * Creates a new empty queue. This method executes before every test.
	 */
	@Before
	public void setupEscenario1()
	{
		queue = new Queue<Taxi>();
	}

	/**
	 * Creates a new queue with 5 taxis. This method executes before every test.
	 */
	public void setupEscenario2( )
	{
		queue = new Queue<Taxi>();
		taxi1 = new Taxi("1234", "Tappsi");
		taxi2 = new Taxi("5678", "Easy Taxi");
		taxi3 = new Taxi("9112", "Taxis Inc.");
		taxi4 = new Taxi("3456", "Cab S.A.");
		taxi5 = new Taxi("1234", "Taxis Plus");

		queue.enqueue(taxi1);
		queue.enqueue(taxi2);
		queue.enqueue(taxi3);
		queue.enqueue(taxi4);
		queue.enqueue(taxi5);	
	}

	// -----------------------------------------------------------------
	// Tests
	// -----------------------------------------------------------------
	
	/**
	 * Test 1: Verifies method enqueue(). <br>
	 * <b>Methods to prove:</b> <br>
	 * Queue<br>
	 * enqueue<br>
	 * dequeue<br>
	 * Cases:
	 * 	Empty queue.
	 * 	Queue with 5 elements.
	 */
	@Test
	public void testEnqueue()
	{

		//Case 1: enqueues  Taxi to an empty queue.
		Taxi taxii= new Taxi("123", "Taxis Mejores S.A.");
		setupEscenario1();

		queue.enqueue(taxii);
		assertEquals("Se deberia de haber incrementado en 1 el tamaño", 1 , queue.size());
		assertEquals("Se deberia de haber agregado el taxi", queue.dequeue(), taxii);

		//Case 2: enqueues  Taxi to a queue with 5 elements.
		Taxi taxiii= new Taxi("123", "Taxis Mejores S.A.");
		setupEscenario2();

		queue.enqueue(taxiii);
		assertEquals("Se deberia de haber incrementado en 1 el tamaño", 6 , queue.size());
	}

	/**
	 * Test 2: Verifies method dequeue(). <br>
	 * <b>Methods to prove:</b> <br>
	 * Queue<br>
	 * dequeue<br>
	 * Cases:
	 * 	Empty queue.
	 * 	Queue with 5 elements.
	 */
	@Test
	public void testDequeue()
	{

		//Case 1: Dequeues an element from an empty queue.
		setupEscenario1();
		try{
			Taxi out = queue.dequeue();
			assertNotNull("No debió de haber eliminado nada", out);
		}
		catch(Exception e)
		{
		}

		//Case 2: Dequeues an element from a queue with 5 elements.

		try{
			queue.dequeue();
			assertEquals("Se deberia de haber disminuido en 1 el tamaño", queue.size() == 4 , queue.size());
			assertEquals("Se deberia de haber eliminado el taxi", taxi1, queue.dequeue());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Test 3: Verifies method isEmpty(). <br>
	 * <b>Methods to prove:</b> <br>
	 * Queue<br>
	 * isEmpty<br>
	 * Cases:
	 * 	Empty queue.
	 * 	Queue is not empty.
	 */
	@Test
	public void testIsEmpty()
	{
		//Case 1: The queue es empty.
		setupEscenario1();

		queue.isEmpty();
		assertTrue("La queue deberia de estar vacía", queue.isEmpty());

		//Case 2: The queue is not empty.
		setupEscenario2();

		assertFalse("La queue no debería estar vacía", queue.isEmpty());
	}
}
