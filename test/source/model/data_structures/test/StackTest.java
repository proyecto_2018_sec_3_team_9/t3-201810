package model.data_structures.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Stack;
import model.vo.Taxi;

public class StackTest 
{
	/**
	 * A new Stack.
	 */
	private Stack<Taxi> stack;

	/**
	 * A new taxi1 in the stack.
	 */
	private Taxi taxi1;
	
	/**
	 * A new taxi2 in the stack.
	 */
	private Taxi taxi2;
	
	/**
	 * A new taxi3 in the stack.
	 */
	private Taxi taxi3;
	
	/**
	 * A new taxi4 in the stack.
	 */
	private Taxi taxi4;
	
	/**
	 * A new taxi5 in the stack.
	 */
	private Taxi taxi5;

	// -----------------------------------------------------------------
	// Escenaries
	// -----------------------------------------------------------------

	/**
	 * Creates a new empty stack. This method executes before every test.
	 */
	@Before
	public void setupEscenario1()
	{
		stack = new Stack<Taxi>();
	}

	/**
	 * Creates a new stack with 5 taxis. This method executes before every test.
	 */
	public void setupEscenario2( )
	{
		stack = new Stack<Taxi>();
		taxi1 = new Taxi("1234", "Tappsi");
		taxi2 = new Taxi("5678", "Easy Taxi");
		taxi3 = new Taxi("9112", "Taxis Inc.");
		taxi4 = new Taxi("3456", "Cab S.A.");
		taxi5 = new Taxi("1234", "Taxis Plus");

		stack.push(taxi1);
		stack.push(taxi2);
		stack.push(taxi3);
		stack.push(taxi4);
		stack.push(taxi5);
	}

	// -----------------------------------------------------------------
		// Tests
		// -----------------------------------------------------------------
		
		/**
		 * Test 1: Verifies method push(). <br>
		 * <b>Methods to prove:</b> <br>
		 * Stack<br>
		 * push<br>
		 * pop<br>
		 * Cases:
		 * 	Empty stack.
		 * 	Stack with 5 elements.
		 */
		@Test
		public void testPush()
		{

			//Case 1: pushes a Taxi to an empty stack.
			Taxi taxii= new Taxi("123", "Taxis Mejores S.A.");
			setupEscenario1();

			stack.push(taxii);
			assertEquals("Se deberia de haber incrementado en 1 el tamaño", 1 , stack.size());
			assertEquals("Se deberia de haber agregado el taxi", stack.pop(), taxii);

			//Case 2: enqueues  Taxi to a queue with 5 elements.
			Taxi taxiii= new Taxi("123", "Taxis Mejores S.A.");
			setupEscenario2();

			stack.push(taxiii);
			assertEquals("Se deberia de haber incrementado en 1 el tamaño", 6 , stack.size());
			assertEquals("Se deberia de haber agregado el taxi", stack.pop(), taxiii);
		}

		/**
		 * Test 2: Verifies method pop(). <br>
		 * <b>Methods to prove:</b> <br>
		 * Stack<br>
		 * pop<br>
		 * Cases:
		 * 	Empty stack.
		 * 	Stack with 5 elements.
		 */
		@Test
		public void testPop()
		{

			//Case 1: Dequeues an element from an empty queue.
			setupEscenario1();
			try{
				Taxi out = stack.pop();
				assertNotNull("No debió de haber eliminado nada", out);
			}
			catch(Exception e)
			{
			}

			//Case 2: Dequeues an element from a queue with 5 elements.

			try{
				stack.pop();
				assertEquals("Se deberia de haber disminuido en 1 el tamaño", 4 , stack.size());
				assertEquals("Se deberia de haber eliminado el taxi", taxi1, stack.pop());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		/**
		 * Test 3: Verifies method isEmpty(). <br>
		 * <b>Methods to prove:</b> <br>
		 * Stack<br>
		 * isEmpty<br>
		 * Cases:
		 * 	Empty stack.
		 * 	Stack is not empty.
		 */
		@Test
		public void testIsEmpty()
		{
			//Case 1: The stack es empty.
			setupEscenario1();

			assertTrue("La queue deberia de estar vacía", stack.isEmpty());

			//Case 2: The queue is not empty.
			setupEscenario2();

			assertFalse("La queue no debería estar vacía", stack.isEmpty());
		}
}
